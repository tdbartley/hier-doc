\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2018

% ready for submission
%\usepackage{nips_2018}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
%\usepackage[preprint]{nips_2018}

% to compile a camera-ready version, add the [final] option, e.g.:
%\usepackage[final]{nips_2018}

% to avoid loading the natbib package, add option nonatbib:
%     \usepackage[nonatbib]{nips_2018}

\usepackage[margin=0.8in,tmargin=0.2in]{geometry}
\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{microtype}      % microtypography
\usepackage{amsmath}
\pagenumbering{gobble}
%\usepackage{algorithmic}

\title{Using Model-free Policies for Temporal Abstraction in \\ Model-based Planning}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.

\author{%
  Travis Bartley, Emre Neftci \\
  The University of California, Irvine\\
  % examples of more authors
  % \AND
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
}

\date{}

\begin{document}
% \nipsfinalcopy is no longer used

\maketitle

%% \begin{abstract}
%% \end{abstract}


\section*{Abstract}

The ability to plan is vital for solving environments where the
outcome of a single action may not be observed until many subsequent
actions have been taken. Using an internal model of the environment,
agents can simulate their actions and predict the outcome, allowing
them to plan action sequences that maximize reward far into the
future. Planning provides two main benefits: 1) it enables the agent
to find sparse rewards more reliably, and 2) it enables generalization
to unseen scenarios composed of familiar
entities~\cite{DBLP:journals/corr/KanskySMELLDSPG17}. For these
reasons, planning is an important cognitive ability for solving
complex and changing environments.

Monte Carlo tree search
(MCTS)~\cite{chaslot_winands_uiterwijk_herik_bouzy_2007} is a
well-known and versatile algorithm used for planning, and algorithms
with world champion-level performance on discrete Markov Decision
Processes (MDPs) such as chess and Go rely on MCTS.  However,
state-of-the-art model-based planning algorithms such as MCTS are slow
or intractable in complex environments such as the Arcade Learning
Environment, since planning must be performed over very large
sequences of state-action pairs. However, many of these state-action
pairs have only minor differences, so it would be unnecessary to plan
over such trivial steps.

To plan over only a small number of the most interesting samples
within the state space, we propose to use options~\cite{SUTTON1999181}
for temporal abstraction.  Options are extended courses of action that
are carried out over many base-level time steps. Using this framework,
our agent is implemented hierarchically using option policies
$\pi_\omega(a \mid s)$ for acting on base-level time steps, and a
policy over options $\pi_\Omega(\omega \mid s)$ for selecting options
on a longer timescale. In addition, each option policy has a
corresponding termination function $\beta_\omega(s')$ which determines
when the current option should terminate and the next option should be
selected. We use the option-critic architecture~\cite{Bacon2016TheOA}
to train a multi-headed neural network to estimate the option
policies, the termination functions, and the policy over options.

We extend the option-critic architecture by using MCTS to plan over
options, rather than using the output of $\pi_\Omega$ directly.
In this case, the edges of the tree correspond to options in a
Semi-Markov Decision Process, rather than actions in a MDP. In the expansion phase of MCTS, a
candidate option is carried out with $\pi_\omega(a \mid s)$ acting on
a simulated environment until $\beta_\omega(s')$ terminates. The tree
is then expanded with a new node representing the state resulting from
that option.

For demonstration, we use the hierarchical agent to solve a waterworld
maze
environment. The environment has complex continuous-state dynamics, including inelastic collisions with walls which can be utilized by a skill player to conserve speed through turns. Each episode is unique, and the player must navigate to the target
in a random maze before time runs out. The
complex dynamics together with the reasoning and planning required to
solve the environment makes it challenging for both model-free and
model-based reinforcement algorithms. Model-free agents are unable to
extract the principles of how the environment works so that they can
generalize to new instances. On the other hand, the continuous state
space is challenging for model-based planning agents, as planning
would have to be performed over very long action sequences. Therefore,
in solving the environment, we demonstrate that the hierarchical
algorithm combines the efficiency of model-free algorithms with the
strength and generalization of model-based algorithms.



\small
\bibliography{references}
\bibliographystyle{unsrt}

\end{document}
