%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[landscape,3040,fontscale=0.300]{baposter} % Adjust the font scale/size here

\usepackage{graphics} % Required for including images
\usepackage{grfext}
\PrependGraphicsExtensions*{.pdf,.PDF}

\usepackage{acronym}
\graphicspath{{img/}{img/}} % Directory in which figures are stored

\usepackage{amsmath} % For typesetting math
\usepackage{amssymb} % Adds new symbols to be used in math mode

\usepackage{booktabs} % Top and bottom rules for tables
\usepackage{enumitem} % Used to reduce itemize/enumerate spacing
%\usepackage{palatino} % Use the Palatino font
\usepackage[font=small,labelfont=bf,textfont={it,footnotesize},labelformat=empty]{caption} % Required for specifying captions to tables and figures

\usepackage{multicol} % Required for multiple columns
\setlength{\columnsep}{1.5em} % Slightly increase the space between columns
\setlength{\columnseprule}{0mm} % No horizontal rule between columns
\usepackage[numbers]{natbib}

\usepackage{tikz} % Required for flow chart
\usetikzlibrary{shapes,arrows} % Tikz libraries required for the flow chart in the template

\newcommand{\compresslist}{ % Define a command to reduce spacing within itemize/enumerate environments, this is used right after \begin{itemize} or \begin{enumerate}
\setlength{\itemsep}{1pt}
\setlength{\parskip}{0pt}
\setlength{\parsep}{0pt}
}
\definecolor{nmigreen}{HTML}{B2BA67}
\definecolor{lightblue}{rgb}{0.145,0.6666,1} % Defines the color used for content box headers

\newlength\Colsep
\setlength\Colsep{8pt}

\input{acronyms}

\newcommand{\refeq}[1]{{Eq.~(\ref{#1})}}
\newcommand{\refeqs}[1]{{Eqs.~(\ref{#1})}}

\begin{document}

\begin{poster}
{
headerborder=none, % Adds a border around the header of content boxes
columns=7,
colspacing=1em, % Column spacing
bgColorOne=white, % Background color for the gradient on the left side of the poster
bgColorTwo=white, % Background color for the gradient on the right side of the poster
borderColor=white, % Border color
headerColorOne=nmigreen, % Background color for the header in the content boxes (left side)
headerColorTwo=nmigreen, % Background color for the header in the content boxes (right side)
headerFontColor=black, % Text color for the header text in the content boxes
boxColorOne=white, % Background color of the content boxes
textborder=rounded, % Format of the border around content boxes, can be: none, bars, coils, triangles, rectangle, rounded, roundedsmall, roundedright or faded
eyecatcher=true, % Set to false for ignoring the left logo in the title and move the title left
headerheight=0.1\textheight, % Height of the header
headershape=smallrounded, % Specify the rounded corner in the content box headers, can be: rectangle, small-rounded, roundedright, roundedleft or rounded
headerfont=\large\bf\textsc, % Large, bold and sans serif font in the headers of content boxes
%textfont={\setlength{\parindent}{1.5em}}, % Uncomment for paragraph indentation
linewidth=2pt % Width of the border lines around content boxes
}
%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
%
{
\noindent\begin{minipage}[t][1.5cm][t]{2cm}
%Affiliation logos here
\includegraphics[width=3.5em]{logo_uci_stacked}\\
\includegraphics[width=3.5em]{logo_UCSD}
\end{minipage}
}
{\bf\textsc{Using Model-free Policies for Temporal Abstraction in \\ Model-based Planning}} % Poster title
{\textsc{Travis Bartley$^1$,
Emre Neftci$^2$}\\    % Author names and institution
\small{
$^{1}$Department of Electrical Engineering and Computer Science, UC Irvine, Irvine, CA, USA (tbartley@uci.edu)\\
$^{2}$Department of Cognitive Sciences, UC Irvine, Irvine, CA, USA
}
}
{
    \raggedright
\noindent\begin{minipage}[t][1.3cm][t]{1.3cm}
\includegraphics[width=2.5em]{logo_nmi_text}
\end{minipage}
} % Second university/lab logo on the right

%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------


%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------
\headerbox{Introduction}{name=introduction,column=0,span=2,row=0}{
Recent studies have shown that synaptic unreliability is a robust and sufficient mechanism for inducing the stochasticity observed in cortex.
We introduce the \ac{SSM}, a stochastic neural network model that uses synaptic unreliability as a means to stochasticity for sampling.
}

\headerbox{Probabilistic Synapses}{name=background,span=2,below=introduction,boxColorOne=white}{

\noindent\begin{minipage}{\textwidth}
\begin{minipage}[c][4.5cm][t]{\dimexpr0.45\textwidth-0.5\Colsep\relax}
The unreliable transmission of synaptic vesicles in biological neurons is a major source of stochasticity in the brain.
\begin{center}
\includegraphics[width = 0.9\textwidth]{placeholder}\\

\end{center}
\end{minipage}\hfill
\begin{minipage}[c][4.5cm][t]{\dimexpr0.55\textwidth-0.5\Colsep\relax}
\begin{center}
\includegraphics[width = 0.9\textwidth]{placeholder}\\
\captionof{figure}{Stochastic synapses robustly generate Poisson-like variability over a wide range of firing rates}

\end{center}
\end{minipage}%
\end{minipage}
}

\headerbox{\aclp{SSM}}{name=method,column=0,span=2,below=background,boxColorOne=white}{ % This block's bottom aligns with the bottom of the conclusion block
Uncertainty at the synapse is sufficient in inducing the variability necessary for probabilistic inference in brain-like circuits.
In the \ac{SSM}, synaptic stochasticity plays the dual role of an efficient mechanism for efficient implementation of stochasticity in neuromorphic hardware, and a regularizer during learning akin to DropConnect.

\begin{center}
\includegraphics[width=.75\linewidth]{placeholder}
\end{center}
}

%----------------------------------------------------------------------------------------
%	Materials & Methods Column
%----------------------------------------------------------------------------------------
\headerbox{Synaptic Sampling ($\mathrm{S}^2\mathrm{M}$)}{name=ssm,column=2,span=2,row=0}{
    As in Boltzmann Machines, the SSM introduces a stochastic component to Hopfield networks in order to avoid spurious local minima and over-fitting.
But rather than units themselves being noisy the connections are noisy.\\

{\footnotesize
Restricted Boltzmann Machines
\[
    P_{RBM}(z_i[t+1] |\mathbf{z}[t])= (1+\exp(- \beta \sum_j w_{ij} z_j[t] + b_i))^{-1}
%    \mathrm{erf}\left(- \beta_i \sum_j w_{ij} z_j[t] \right),
\]
$\mathbf{\mathrm{S}^2\mathrm{M}}$: Discrete-time Synaptic Sampling (Threshold neurons):
\[
    P_{\mathrm{S}^2\mathrm{M}}(z_i[t+1]|\mathbf{z}) = \frac12 \left(1 + \mathrm{erf}\left(  \beta    \frac{\sum_j w_{ij} z_j[t] + \frac{b_i}{p}}{\sqrt{\sum_j w_{ij}^2 z_j[t]^2}} \right)\right),
%    \mathrm{erf}\left(- \beta_i \sum_j w_{ij} z_j[t] \right),
\]
\[
\beta  =  \sqrt{\frac{ p }{2(1-p) } }.
\]
%
$\mathbf{\mathrm{S}^3\mathrm{M}}$: Continuous-time Synaptic Sampling (LIF neurons):
\[
    P_{\mathrm{S}^3\mathrm{M}}((z_i(t+1) |\mathbf{z}(t)) \xrightarrow[c\rightarrow 0 ]{}
    (\tau_{ref} + \exp(-\beta (\sigma) u_0 + \gamma (\sigma)))^{-1}
\]
%
}
\begin{center}
\includegraphics[width=.5\textwidth]{placeholder}
\end{center}
}
%----------------------------------------------------------------------------------------
%	Synapse learning model
%----------------------------------------------------------------------------------------
\headerbox{Event-Driven CD}{name=plasticity,below=ssm,column=2,span=2,row=0}{
ECD is an online variation of \ac{CD} based on \ac{STDP}.
\vspace{.5cm}
\begin{center}
\includegraphics[width=0.65\linewidth]{placeholder}\\
\end{center}
}

\headerbox{Unsupervised Learning of MNIST}{name=results,column=4,span=3,row=0}{

\noindent\begin{minipage}{\textwidth}
\begin{minipage}[c][3.cm][t]{\dimexpr0.6\textwidth-0.5\Colsep\relax}
We demonstrate the $\mathbf\mathrm{S}^3\mathrm{M}$ by learning a model of MNIST hand-written digit dataset, and by testing it in recognition and inference tasks.
\acp{SSM} outperform Boltzmann machines, they are more robust to over-fitting, and tend to learn sparser representations.
\end{minipage}
\begin{minipage}[c][3.cm][t]{\dimexpr0.4\textwidth-0.5\Colsep\relax}
\begin{center}
\includegraphics[width=1.0\linewidth]{placeholder}
\end{center}
\end{minipage}
\end{minipage}

\begin{center}
    {\scriptsize
  \begin{tabular}{l  c  c }
    Model, $n_H$ = 500, 60k digits training set                          & MNIST Accuracy \\
    \hline
    $\mathrm{S}^3\mathrm{M}$ + Event-driven \ac{CD} (this work)                               &\textbf{95.6}\%\\
    $\mathrm{S}^3\mathrm{M}$ + Event-driven \ac{CD} + 81.5\% connection pruning + relearning (this work)                               &\textbf{95.4}\%\\
    $\mathrm{S}^3\mathrm{M}$ + Event-driven \ac{CD} + 4 bit synaptic weights (this work)   &94.8\%\\
    $\mathrm{S}^2\mathrm{M}$ + Standard \ac{CD} (this work)                     &\textbf{95.5}\%\\
    RBM + Gibbs Sampling + Standard \ac{CD}                                            &95.0\%\\
    Neural Sampling + Event-driven \ac{CD}                                           &91.9\%\\
    \hline
  \end{tabular}
  }

\end{center}

\begin{center}
\begin{minipage}[c][2.8cm][t]{\dimexpr0.18\textwidth-0.5\Colsep\relax}
\includegraphics[width=1.0\linewidth]{placeholder}
\end{minipage}
\begin{minipage}[c][2.8cm][t]{\dimexpr0.39\textwidth-0.5\Colsep\relax}
\includegraphics[width=1.0\linewidth]{placeholder}
%\captionof{figure}{}
\end{minipage}
\begin{minipage}[c][2.8cm][t]{\dimexpr0.39\textwidth-0.5\Colsep\relax}
\includegraphics[width=1.0\linewidth]{placeholder}
\end{minipage}
\end{center}

}

%----------------------------------------------------------------------------------------
%	RESULTS 2
%----------------------------------------------------------------------------------------

\headerbox{Conclusion}{name=results2,column=4,span=3,below=results}{ % This block's bottom aligns with the bottom of the conclusion block
\noindent\begin{minipage}{\textwidth}
\begin{minipage}[c][3.6cm][t]{\dimexpr0.65\textwidth-0.5\Colsep\relax}
The performance of the \ac{SSM} vastly improves on our previous implementation on learning to recognize the MNIST hand-written digits and on RBMs.
Results suggest that \acp{SSM} also offer substantial improvements in terms of performance, power and complexity over other methods for unsupervised learning in spiking neural networks, and are thus promising models for implementations of deep learning in neuromorphic hardware.
\end{minipage}
\begin{minipage}[c][3.6cm][t]{\dimexpr0.35\textwidth-0.5\Colsep\relax}
\includegraphics[width = 0.9\textwidth]{placeholder}
\end{minipage}
\end{minipage}

%----------------------------------------------------------------------------------------
%	CONCLUSION
%----------------------------------------------------------------------------------------
\hspace{.8cm}
%Our results thus offer a non-linear dynamical systems perspective on how the brain may organize the learning of long sequences of neural activity.
\begin{multicols}{2}
%------------------------------------------------
\columnbreak
%To remove bibliography
\renewcommand{\section}[2]{\vskip 0.00em}
{\tiny
\setlength{\bibsep}{0.0pt}
\bibliographystyle{unsrt5}
\bibliography{/home/eneftci/Projects/lib/bibtex/biblio_unique.bib}
}
{\small
\textbf{E-mail:} eneftci@uci.edu\\
\textbf{Support:} National Science Foundation (NSF CCF-1317373), The Office of Naval Research (ONR MURI 14-13-1-0205), Intel Corporation\\
\vfill
    %Support logos here
    \includegraphics[height = .6cm]{logo_intel}
    \includegraphics[height = .6cm]{logo_nsf}
    \includegraphics[height = .6cm]{ONR_logo}
}

\end{multicols}


}






%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

%\headerbox{References}{name=references,column=2,headerColorOne=white,headerColorTwo=white,aligned=conclusion}{

%\renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
 % Reduce the font size in this block
%}}


\end{poster}

\end{document}
