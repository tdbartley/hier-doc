%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[landscape,3040,fontscale=0.300]{baposter} % Adjust the font scale/size here

\usepackage{graphics} % Required for including images
\usepackage{grfext}
\PrependGraphicsExtensions*{.pdf,.PDF}

\usepackage{acronym}
\graphicspath{{img/}{img/}} % Directory in which figures are stored

\usepackage{amsmath} % For typesetting math
\usepackage{amssymb} % Adds new symbols to be used in math mode

\usepackage{booktabs} % Top and bottom rules for tables
\usepackage{enumitem} % Used to reduce itemize/enumerate spacing
%\usepackage{palatino} % Use the Palatino font
\usepackage[font=small,labelfont=bf,textfont={it,footnotesize},labelformat=empty]{caption} % Required for specifying captions to tables and figures

\usepackage{multicol} % Required for multiple columns
\setlength{\columnsep}{1.5em} % Slightly increase the space between columns
\setlength{\columnseprule}{0mm} % No horizontal rule between columns
\usepackage[numbers]{natbib}

\usepackage{tikz} % Required for flow chart
\usetikzlibrary{shapes,arrows} % Tikz libraries required for the flow chart in the template

\newcommand{\compresslist}{ % Define a command to reduce spacing within itemize/enumerate environments, this is used right after \begin{itemize} or \begin{enumerate}
\setlength{\itemsep}{1pt}
\setlength{\parskip}{0pt}
\setlength{\parsep}{0pt}
}
\definecolor{nmigreen}{HTML}{B2BA67}
\definecolor{lightblue}{rgb}{0.145,0.6666,1} % Defines the color used for content box headers

\newlength\Colsep
\setlength\Colsep{8pt}

\input{acronyms}

\newcommand{\refeq}[1]{{Eq.~(\ref{#1})}}
\newcommand{\refeqs}[1]{{Eqs.~(\ref{#1})}}

\begin{document}

\begin{poster}
{
headerborder=none, % Adds a border around the header of content boxes
columns=6,
colspacing=1em, % Column spacing
bgColorOne=white, % Background color for the gradient on the left side of the poster
bgColorTwo=white, % Background color for the gradient on the right side of the poster
borderColor=white, % Border color
headerColorOne=nmigreen, % Background color for the header in the content boxes (left side)
headerColorTwo=nmigreen, % Background color for the header in the content boxes (right side)
headerFontColor=black, % Text color for the header text in the content boxes
boxColorOne=white, % Background color of the content boxes
textborder=rounded, % Format of the border around content boxes, can be: none, bars, coils, triangles, rectangle, rounded, roundedsmall, roundedright or faded
eyecatcher=true, % Set to false for ignoring the left logo in the title and move the title left
headerheight=0.15\textheight, % Height of the header
headershape=smallrounded, % Specify the rounded corner in the content box headers, can be: rectangle, small-rounded, roundedright, roundedleft or rounded
headerfont=\large\bf\textsc, % Large, bold and sans serif font in the headers of content boxes
%textfont={\setlength{\parindent}{1.5em}}, % Uncomment for paragraph indentation
linewidth=2pt % Width of the border lines around content boxes
}
%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
%
{
\noindent\begin{minipage}[t][1cm][t]{1cm}
%Affiliation logos here
\includegraphics[width=8em]{logo_uci_stacked}\\
\end{minipage}
}
{\bf\textsc{Using Model-free Policies for Temporal Abstraction in \\ Model-based Planning}} % Poster title
{\textsc{Travis Bartley$^1$,
Emre Neftci$^2$}\\    % Author names and institution
\small{
$^{1}$Department of Electrical Engineering and Computer Science, UC Irvine, Irvine, CA, USA\\
$^{2}$Department of Cognitive Sciences, UC Irvine, Irvine, CA, USA
}
}
{
\raggedright
\noindent\begin{minipage}[t][1.3cm][t]{1.3cm}
\includegraphics[width=4em]{logo_nmi_text}
\end{minipage}
} % Second university/lab logo on the right

%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------


%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------
\headerbox{Introduction}{name=introduction,column=0,span=2,row=0}{
  Planning provides two main benefits:
  \begin{enumerate}
    \item it enables the agent
      to find sparse rewards more reliably
    \item it enables generalization
  to unseen scenarios composed of familiar
  entities~\cite{DBLP:journals/corr/KanskySMELLDSPG17}
  \end{enumerate}
  However,
  state-of-the-art model-based planning algorithms are slow
  in complex environments. We propose to use temporal abstraction to make planning more efficient using a hierarchical
  agent.
}

\headerbox{Monte Carlo Tree Search}{name=background,span=2,below=introduction,boxColorOne=white}{
  Monte Carlo tree search
  (MCTS)~\cite{chaslot_winands_uiterwijk_herik_bouzy_2007} is a versatile planning algorithm that simulates actions (edges) and records the resulting states (nodes).
  \begin{center}
    \includegraphics[page=74, clip, trim=1.7cm 15.5cm 1.7cm 2cm, width=0.8\textwidth]{mcts.pdf}\\
  \end{center}
}

\headerbox{Temporal Abstraction}{name=method,column=0,span=2,below=background,boxColorOne=white}{ % This block's bottom aligns with the bottom of the conclusion block
  To make planning more efficient, we use the options framework~\cite{SUTTON1999181}
  for temporal abstraction. This allows planning over a Semi-Markov Decision Process.
  \begin{center}
    \includegraphics[page=4, clip, trim=7cm 19.5cm 7cm 3cm, width=0.6\textwidth]{mdp_smdp.pdf}\\
  \end{center}
}

%----------------------------------------------------------------------------------------
%	Materials & Methods Column
%----------------------------------------------------------------------------------------
\headerbox{Hierarchical Agent}{name=ssm,column=2,span=2,row=0}{
  We use a manager-worker arrangement, where the manager plans over options $\omega$. The options are then taken as input to the worker network which controls the actions $a$ of the agent. During the planning phase, the actions are performed in a simulated environment $M$, and during execution, actions are performed on the real environment. There is also a visual encoder $V$ which encodes the observations $s$ to states $z$.
  \begin{center}
    \includegraphics[width=0.8\textwidth]{hierarchy_full.pdf}
  \end{center}
}
%----------------------------------------------------------------------------------------
%	Synapse learning model
%----------------------------------------------------------------------------------------
\headerbox{Random Maze Benchmark}{name=plasticity,below=ssm,column=2,span=2,row=0}{
  We use a waterworld
  maze
  environment as our benchmark. The environment has complex continuous-state dynamics, including inelastic collisions with walls. Each episode is unique, and the player must navigate to the target
  in a random maze before time runs out. The
  complex dynamics together with the reasoning and planning required to
  solve the environment makes it challenging for both model-free and
  model-based reinforcement algorithms.

  \vspace{.1cm}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{mazes.png}\\
  \end{center}

  The figure above illustrates the starting positions for eight episodes with random placement of the player (blue), the target (green) and walls (black).
}

\headerbox{Results}{name=results,column=4,span=2,row=0}{

  Below illustrates the hierarchical agent on 2 episodes (in sequence left to right). The options (red) are pre-defined as move-one-tile-up, move-one-tile-down, move-one-tile-left, and move-one-tile-right.
  \vspace{.1cm}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{hier1.png}\\
  \end{center}
  %\vspace{.1cm}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{hier2.png}\\
  \end{center}

  The results below show that the model-free agent (A3C) is unable to
  extract the principles of how the environment works. On the other hand, the hierarchical agent is able to solve the mazes since it has a model of the environment.


  \begin{center}
    \begin{minipage}[c][3cm][t]{\dimexpr0.39\textwidth-0.5\Colsep\relax}
      \begin{center}
        A3C agent
      \end{center}
      \includegraphics[width=1.0\linewidth]{a3c_results.png}
      %\captionof{figure}{}
    \end{minipage}
    \begin{minipage}[c][3cm][t]{\dimexpr0.39\textwidth-0.5\Colsep\relax}
      \begin{center}
        Hierarchical agent
      \end{center}
      \includegraphics[width=1.0\linewidth]{hier_results.png}
    \end{minipage}
  \end{center}

The performance of the hierarchical reinforcement learning agent demonstrates a successful combination of the efficiency of model-free algorithms with the
  strength and generalization of model-based algorithms.

%----------------------------------------------------------------------------------------
%	RESULTS 2
%----------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------
%	CONCLUSION
%----------------------------------------------------------------------------------------
%\hspace{.8cm}
%Our results thus offer a non-linear dynamical systems perspective on how the brain may organize the learning of long sequences of neural activity.
\begin{multicols}{2}
%------------------------------------------------
\columnbreak
%To remove bibliography
\renewcommand{\section}[2]{\vskip 0.00em}
{\tiny
\setlength{\bibsep}{0.0pt}
\bibliographystyle{unsrt5}
\bibliography{references.bib}
}
{\small
  \vspace{0.4cm}
\textbf{E-mail:} tbartley@uci.edu\\
\textbf{Support:} Defense Advanced Research Projects Agency, Lifelong Learning Machines Program\\
\vfill
    %Support logos here
%\includegraphics[height = .6cm]{logo_darpa}
    %% \includegraphics[height = .6cm]{logo_nsf}
    %% \includegraphics[height = .6cm]{ONR_logo}
}

\end{multicols}


}






%----------------------------------------------------------------------------------------
%	REFERENCES
%----------------------------------------------------------------------------------------

%\headerbox{References}{name=references,column=2,headerColorOne=white,headerColorTwo=white,aligned=conclusion}{

%\renewcommand{\section}[2]{\vskip 0.05em} % Get rid of the default "References" section title
 % Reduce the font size in this block
%}}


\end{poster}

\end{document}
