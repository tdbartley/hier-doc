\documentclass{article} % For LaTeX2e
%\usepackage{iclr2019_conference,times}

% Optional math commands from https://github.com/goodfeli/dlbook_notation.
\input{math_commands.tex}

\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{algorithm, algorithmic}

\title{Hierarchical Reinforcement Learning with Model-Based Planning for Sparse Rewards}

\author{Travis Bartley, Emre Neftci}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

%\iclrfinalcopy % Uncomment for camera-ready version, but NOT for submission.
\begin{document}


\maketitle

%\begin{abstract}
%The abstract paragraph should be indented 1/2~inch (3~picas) on both left and
%right-hand margins. Use 10~point type, with a vertical spacing of 11~points.
%The word \textsc{Abstract} must be centered, in small caps, and in point size 12. Two
%line spaces precede the abstract. The abstract must be limited to one
%paragraph.
%\end{abstract}

\section{Introduction}
In classical artificial intelligence methods, the navigation problem is separated into two phases: \textit{exploration} and \textit{exploitation}. In the exploration phase, the agent moves around in an attempt to learn an internal representation of the environment. This enables the agent to plan efficient actions according to some criterion, such as minimizing distance or energy consumption in the exploitation phase.

On the other hand, deep reinforcement learning (DRL) allows for end-to-end navigation methods, where there is no clear separation into these two phases. These methods have gained traction due to recent advancements~\cite{DBLP:conf/icml/MnihBMGLHSK16, silver_huang_maddison_guez_sifre_driessche_schrittwieser_antonoglou_panneershelvam_lanctot_etal_2016, DBLP:journals/corr/MirowskiPVSBBDG16}. The appeal of such approaches is generality. The manual extraction of input features and the manual design of map-building and path-planning algorithms can be avoided by instead formulating a reward function as a proxy for the navigation objective. The DRL agent then learns intermediate map representations that enable it to maximize its reward.

However, a recent investigation reveals significant shortcomings of A3C, which is the state-of-the-art DRL navigation algorithm~\cite{DBLP:journals/corr/abs-1802-02274}. Experiments show that while the algorithm is able to efficiently exploit map information when trained and tested on the same map, it is unable to do so when trained and tested on different maps. Even when tested and trained on the same map, the performance of the algorithm deteriorates when the placement of the destination is randomized. These observations indicate that A3C does not learn to solve the general problem of maze navigation, but only the much narrower task of navigating in environments it has been trained on. DRL also shows very poor ability to generalize over multiple tasks, as in Atari 2600 games~\cite{DBLP:journals/corr/KanskySMELLDSPG17}. It was shown that the performance of these algorithms drastically deteriorates after the positions of objects were subtly perturbed. This indicates that even state-of-the-art DRL models still rely on exploiting brittle stimulus-response mappings. While DRL agents may outperform humans on narrow tasks they have previously trained on, there is still a long way to go until human-level generalization across tasks is achieved.

The general maze navigation problem can be thought of as a sequential multi-task problem. In this case, each task is a different random maze composed of a fixed set of entities (walls, agent, destination). After learning the principles of navigation by training on some set of mazes, the agent should be able to transfer its knowledge to solve new unseen mazes efficiently. Therefore, transfer learning between tasks~\cite{Taylor:2009:TLR:1577069.1755839} is required for solving the general maze navigation problem.

To achieve transfer learning in video game playing, Kansky~\textit{el.~al.}~\cite{DBLP:journals/corr/KanskySMELLDSPG17} argue that 1) \textbf{object-based representations} can be used to exploit the structure of the domain, 2) a \textbf{causal model of the environment} is necessary for planning, and 3) \textbf{goal-oriented planning} enables generalization to new environments composed of familiar elements. To this end, the authors developed a Probabilistic Graphical Model (PGM) named Schema Networks, which was able to substantially generalize beyond its training experience. These findings need not be limited to PGMs, but can be readily applied create new DRL algorithms which can transfer experiences between similar tasks.

The approach proposed in this paper is to solve the exploitation phase of the general maze navigation problem using DRL, by adopting the above three  conclusions that were reached by Kansky~\textit{el.~al.}. To exploit the structure of the domain, we use object-based representations from the environment state. Using environment states directly could be avoided by using a vision system for detecting and tracking entities in an image, e.g. a Capsule Network~\cite{DBLP:journals/corr/abs-1710-09829}. Object-based representations enable the agent to model the environment accurately using an interaction network~\cite{DBLP:journals/corr/BattagliaPLRK16}. This, in turn, allows the agent to plan its actions according to a Monte Carlo tree search (MCTS)~\cite{chaslot_winands_uiterwijk_herik_bouzy_2007}.


\section{Model}

\begin{itemize}
    \item Visual encoder (CNN~\cite{DBLP:journals/corr/WattersTWPBZ17} or convolutional autoencoder~\cite{DBLP:journals/corr/GarneloAS16})
    \begin{itemize}
        \item In: RGB image sequence, $x_0, x_1, \ldots, x_t$
        \item Out: state code
    \end{itemize}
    \item Dynamics predictor (interaction-net cores and MLP aggregator~\cite{DBLP:journals/corr/WattersTWPBZ17})
    \begin{itemize}
        \item In: state code
        \item Out: predicted state code
    \end{itemize}
    \item State decoder (linear layer~\cite{DBLP:journals/corr/WattersTWPBZ17})
    \begin{itemize}
        \item In: predicted state code
        \item Out: predicted environment state, $\hat{s}$
    \end{itemize}
    \item Policy network
    \begin{itemize}
        \item In: environment state, $s$ or  $\hat{s}$
        \item Out: policy, $p(a|s)$
    \end{itemize}
    \item Value network
    \begin{itemize}
        \item In: environment state, $s$ or  $\hat{s}$
        \item Out: reward estimate, $v(s)$
    \end{itemize}
\end{itemize}

\begin{tabular}{|l|l|}
 \hline
 Variable & Meaning\\ \hline
 $\bm{\theta}_m$, $\bm{\theta}_m'$ & Global, local manager policy parameters \\ \hline
 $\bm{\theta}_{mv}$, $\bm{\theta}_{mv}'$ & Global, local manager value parameters \\ \hline
 $\bm{\theta}_w$, $\bm{\theta}_w'$ & Global, local worker policy parameters \\ \hline
 $\bm{\theta}_{wv}$, $\bm{\theta}_{wv}'$ & Global, local worker value parameters \\ \hline
\end{tabular}

\begin{algorithm}
\caption{Pseudocode for each hierarchical learner thread}
\label{alg1}
\begin{algorithmic}
    \STATE // Assume global parameter vectors $\bm{\theta}_m$, $\bm{\theta}_{mv}$, $\bm{\theta}_w$ and $\bm{\theta}_{wv}$ and global counter $T=0$
    \STATE // Assume thread-specific parameter vectors $\bm{\theta}_m'$, $\bm{\theta}_{mv}'$, $\bm{\theta}_w'$ and $\bm{\theta}_{wv}'$
    \STATE Initialize thread step counter $t \leftarrow 1$
    \WHILE{$T \leq T_{max}$}
    \STATE Reset gradients: $d\bm{\theta}_m \leftarrow 0$, $d\bm{\theta}_{mv} \leftarrow 0$, $d\bm{\theta}_w \leftarrow 0$ and $d\bm{\theta}_{wv} \leftarrow 0$ 
    \STATE Synchronize thread-specific parameters $\bm{\theta}_m' = \bm{\theta}_m$, $\bm{\theta}_{wv}' = \bm{\theta}_{wv}$, $\bm{\theta}_w' = \bm{\theta}_w$ and $\bm{\theta}_{wv}' = \bm{\theta}_{wv}$
    \STATE $t_{start} = t$
    \STATE Get state $s_t$
    \WHILE{not terminal $s_t$ AND $t-t_{start} < t_{max}$}
        \STATE $\bm{G}, r_{wt} = f_m(s_t, \bm{\theta}_m')$ // Manager function
        \STATE Perform $a_t$ according to policy $\pi (a_t | s_t, \bm{G}; \bm{\theta}_w')$
        \STATE Receive reward $r_{mt}$ and new state $s_{t+1}$
        \STATE $t \leftarrow t + 1$
        \STATE $T \leftarrow T + 1$
    \ENDWHILE
    \STATE $R_m, R_w = \begin{cases} 
                    0, 0 & \text{for terminal } s_t \\
                    V(s_t, \bm{\theta}_{mv}'), V(s_t, \bm{g}, \bm{\theta}_{wv}') & \text{for non-terminal } s_t \text{ // Bootstrap from last state}
                \end{cases}$
    \FOR{$i \in \{t-1,\ldots,t_{start}\}$}
    \STATE $R_m \leftarrow r_{mi}+ \gamma R_m$, $R_w \leftarrow r_{wi}+ \gamma R_w$
    \STATE Accumulate gradients wrt $\bm{\theta}_m': d\bm{\theta}_m \leftarrow d\bm{\theta}_m + \nabla_{\bm{\theta}_m'} \log \pi(a_i | s_i;\bm{\theta}_m')(R_m-V(s_i;\bm{\theta}_{mv}'))$
    \STATE Accumulate gradients wrt $\bm{\theta}_{mv}': d\bm{\theta}_{mv} \leftarrow d\bm{\theta}_{mv} + \partial (R_m-V(s_i;\bm{\theta}_{mv}'))^2 / \partial \bm{\theta}_{mv}'$
    \STATE Accumulate gradients wrt $\bm{\theta}_w': d\bm{\theta}_w \leftarrow d\bm{\theta}_w + \nabla_{\bm{\theta}_w'} \log \pi(a_i | s_i;\bm{\theta}_w')(R_w-V(s_i;\bm{\theta}_{wv}'))$
    \STATE Accumulate gradients wrt $\bm{\theta}_{wv}': d\bm{\theta}_{wv} \leftarrow d\bm{\theta}_{wv} + \partial (R_w-V(s_i;\bm{\theta}_{wv}'))^2 / \partial \bm{\theta}_{wv}'$
    \ENDFOR
    \STATE Perform asynchronous update of $\bm{\theta}_w$ using $ d\bm{\theta}_w$ and of $\bm{\theta}_{wv}$ using $d\bm{\theta}_{wv}$
    \ENDWHILE
\end{algorithmic}
\end{algorithm}

\begin{algorithm}
\caption{Pseudocode for manager function}
\label{alg2}
\begin{algorithmic}
    \STATE // Assume Euclidean distance between two latent environment states $d(x, y)$
    \STATE // Assume $\mathrm{do\_planning}$ initialized to TRUE before first execution
    \STATE $r_{wt} \leftarrow r_{\mathrm{tick}}$
    \IF{$\mathrm{do\_planning}$}
    \STATE $\mathrm{do\_planning} \leftarrow \mathrm{FALSE}$
    \STATE $\bm{G} \leftarrow \mathrm{tree\_search}(s_t)$ // Perform MCTS to produce goal matrix
    \ENDIF
    \IF{$d(\bm{g}_0, s_t) < \mathrm{goal\_met\_thr}$}
    \STATE $r_{wt} \leftarrow r_{wt} + r_{\mathrm{goal\_met}}$ // Reward for reaching goal
    \ENDIF
    \IF{$d(\bm{g}_0, s_t) > \mathrm{goal\_infeasible\_thr}$}
    \STATE $\mathrm{do\_planning} \leftarrow \mathrm{TRUE}$
    \STATE $r_{wt} \leftarrow r_{wt} + r_{\mathrm{goal\_infeasible}}$ // Punishment for diverging from goal
    \ENDIF
\end{algorithmic}
\end{algorithm}


\section{Experiments}

\begin{itemize}
    \item A training set of 1000 mazes were generated according to algorithm~\cite{rougier_2010}
    \item A testing set of 100 mazes were generated in the same manner
    \item GridWorld-style $9\times9$ environment made using PyGame Learning Environment~\cite{tasfi2016PLE}
    \begin{itemize}
        \item Actions: up, down, left, right, no-op
        \item Rewards: $r_{\text{win}}=5.0$, $r_{\text{loss}}=-5.0$, $r_{\text{positive}}=1.0$, $r_{\text{tick}}=-0.1$
        \item Time limit: 1000 ticks
        \item Maze, starting position and reward position are randomized for each training/testing instance
        \item Agent has complete observability of the environment (evaluating only exploitation phase of maze navigation)
    \end{itemize}

\end{itemize}


\subsection{Baseline Model}
\begin{itemize}
    \item A3C
\end{itemize}



\bibliography{references}
\bibliographystyle{unsrt}

\end{document}
