\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2018

% ready for submission
% \usepackage{nips_2018}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
%     \usepackage[preprint]{nips_2018}

% to compile a camera-ready version, add the [final] option, e.g.:
%\usepackage{nips_2018}

% to avoid loading the natbib package, add option nonatbib:
%     \usepackage[nonatbib]{nips_2018}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{algorithm}
\usepackage{algorithmic}

\title{February 2019 Monthly Report:\\
  Hierarchical Learner for Finding Sparse Rewards}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.

\author{%
  Travis Bartley, Emre O. Neftci, Jeffrey L. Krichmar
}

\begin{document}
% \nipsfinalcopy is no longer used

\maketitle

%% \begin{abstract}
%%   Through model-based planning, agents can internally simulate their
%%   actions and predict the outcome, allowing them to discover an action
%%   sequence that maximizes future reward.
%%   However, the computational cost of planning is orders of
%%   magnitude more than that of simple stimulus-response neural
%%   networks. This work introduces cognitive control
%%   for combining the strength and
%%   flexibility of planning together with the efficiency of automatic
%%   processes.
%% \end{abstract}


\section{Background}

Through model-based planning, agents can internally simulate their
actions and predict the outcome, allowing them to discover an action
sequence that maximizes future reward.
However, the computational cost of planning is orders of
magnitude more than that of simple stimulus-response neural
networks. This is due to the fact that hundreds of time steps of internal
simulations may be needed to discover a suitable action
sequence. Relying on fully planning at every time step is not only inefficient
but unnecessary. On the other hand, without planning, the agent's
ability to generalize will be impaired, and the quality of it's
action selection will diminish.

To achieve both computational efficiency and strong generalization,
we propose a hierarchical learner with separate modules for planning
and action selection. The planning module initially sets a plan
consisting of a series of goal state vectors, $\bm{g}_i$, which are represented by matrix
$G$. These goal vectors correspond to some desired future state of the
environment, and exist in some learned latent space. The goals are much less
sparse than the rewards in the environment, but are sparse enough
that tens of frames may elapse before a goal can be reached. The
controller then takes these goals as input, and is
responsible for providing frame-by-frame actions in attempt to reach
the goal. If the controller gets close to the goal, it receives a
reward, and if it strays too far from the goal it receives a
punishment. The pseudocode for the overall hierarchical learner and
the manager function are shown in Algorithm~\ref{algo:global} and
Algorithm~\ref{algo:manager} below.

The benefit of this hierarchical system is that planning is
performed only periodically, since the controller responds to the
short-term changes in environment in pursuit of the goals. Rather
than performing planning at every step, planning is only performed
when one of the following conditions is met:
\begin{itemize}
\item a new episode begins
\item all previous goals have been reached
\item the next goal has become unobtainable (either from the controller
  straying too far from the goal, or due to an unexpected change in
  the environment)
\end{itemize}


\begin{algorithm}\label{algo:global}
\caption{Pseudocode for each hierarchical learner thread}
\label{alg1}
\begin{algorithmic}
    \STATE // Assume global parameter vectors $\bm{\theta}_m$, $\bm{\theta}_{mv}$, $\bm{\theta}_w$ and $\bm{\theta}_{wv}$ and global counter $T=0$
    \STATE // Assume thread-specific parameter vectors $\bm{\theta}_m'$, $\bm{\theta}_{mv}'$, $\bm{\theta}_w'$ and $\bm{\theta}_{wv}'$
    \STATE Initialize thread step counter $t \leftarrow 1$
    \WHILE{$T \leq T_{max}$}
    \STATE Reset gradients: $d\bm{\theta}_m \leftarrow 0$, $d\bm{\theta}_{mv} \leftarrow 0$, $d\bm{\theta}_w \leftarrow 0$ and $d\bm{\theta}_{wv} \leftarrow 0$
    \STATE Synchronize thread-specific parameters $\bm{\theta}_m' = \bm{\theta}_m$, $\bm{\theta}_{wv}' = \bm{\theta}_{wv}$, $\bm{\theta}_w' = \bm{\theta}_w$ and $\bm{\theta}_{wv}' = \bm{\theta}_{wv}$
    \STATE $t_{start} = t$
    \STATE Get state $s_t$
    \WHILE{not terminal $s_t$ AND $t-t_{start} < t_{max}$}
        \STATE $\bm{G}, r_{wt} = f_m(s_t, \bm{\theta}_m')$ // Manager function
        \STATE Perform $a_t$ according to policy $\pi (a_t | s_t, \bm{G}; \bm{\theta}_w')$
        \STATE Receive reward $r_{mt}$ and new state $s_{t+1}$
        \STATE $t \leftarrow t + 1$
        \STATE $T \leftarrow T + 1$
    \ENDWHILE
    \STATE $R_m, R_w = \begin{cases}
                    0, 0 & \text{for terminal } s_t \\
                    V(s_t, \bm{\theta}_{mv}'), V(s_t, \bm{g}, \bm{\theta}_{wv}') & \text{for non-terminal } s_t \text{ // Bootstrap from last state}
                \end{cases}$
    \FOR{$i \in \{t-1,\ldots,t_{start}\}$}
    \STATE $R_m \leftarrow r_{mi}+ \gamma R_m$, $R_w \leftarrow r_{wi}+ \gamma R_w$
    \STATE Accumulate gradients wrt $\bm{\theta}_m': d\bm{\theta}_m \leftarrow d\bm{\theta}_m + \nabla_{\bm{\theta}_m'} \log \pi(a_i | s_i;\bm{\theta}_m')(R_m-V(s_i;\bm{\theta}_{mv}'))$
    \STATE Accumulate gradients wrt $\bm{\theta}_{mv}': d\bm{\theta}_{mv} \leftarrow d\bm{\theta}_{mv} + \partial (R_m-V(s_i;\bm{\theta}_{mv}'))^2 / \partial \bm{\theta}_{mv}'$
    \STATE Accumulate gradients wrt $\bm{\theta}_w': d\bm{\theta}_w \leftarrow d\bm{\theta}_w + \nabla_{\bm{\theta}_w'} \log \pi(a_i | s_i;\bm{\theta}_w')(R_w-V(s_i;\bm{\theta}_{wv}'))$
    \STATE Accumulate gradients wrt $\bm{\theta}_{wv}': d\bm{\theta}_{wv} \leftarrow d\bm{\theta}_{wv} + \partial (R_w-V(s_i;\bm{\theta}_{wv}'))^2 / \partial \bm{\theta}_{wv}'$
    \ENDFOR
    \STATE Perform asynchronous update of $\bm{\theta}_w$ using $ d\bm{\theta}_w$ and of $\bm{\theta}_{wv}$ using $d\bm{\theta}_{wv}$
    \ENDWHILE
\end{algorithmic}
\end{algorithm}


\begin{algorithm}
\caption{Pseudocode for manager function}\label{algo:manager}
\begin{algorithmic}
    \STATE // Assume Euclidean distance between two latent environment states $d(x, y)$
    \STATE // Assume $\mathrm{do\_planning}$ initialized to TRUE before first execution
    \STATE $r_{wt} \leftarrow r_{\mathrm{tick}}$
    \IF{$\mathrm{do\_planning}$}
    \STATE $\mathrm{do\_planning} \leftarrow \mathrm{FALSE}$
    \STATE $\bm{G} \leftarrow \mathrm{tree\_search}(s_t)$ // Perform MCTS to produce goal matrix
    \ENDIF
    \IF{$d(\bm{g}_0, s_t) < \mathrm{goal\_met\_thr}$}
    \STATE $r_{wt} \leftarrow r_{wt} + r_{\mathrm{goal\_met}}$ // Reward for reaching goal
    \ENDIF
    \IF{$d(\bm{g}_0, s_t) > \mathrm{goal\_infeasible\_thr}$}
    \STATE $\mathrm{do\_planning} \leftarrow \mathrm{TRUE}$
    \STATE $r_{wt} \leftarrow r_{wt} + r_{\mathrm{goal\_infeasible}}$ // Punishment for diverging from goal
    \ENDIF
\end{algorithmic}
\end{algorithm}


\section{Progress Summary}

%Prior to this month's progress, a proof of concept implementation was
%demonstrated on a randomized maze environment. The hierarchical agent
%was shown to generalize better than an agent that could not plan, and
%attained a much higher performance. Those prior results are omitted
%for this report.


\subsection{Extending experimental results}

The major challenge at this point is to extend and improve the
hierarchical agent so that it can be used on a wider variety of
environments.


\subsection{Learning the environment transition model}

\begin{itemize}
\item Need appropriate time horizon, frame-by-frame is too granular
  and not meaningful, would blow up tree search
\item MHDPA is the current target since the time-horizon is on the
  granularity of entities
\item Testing environments: Random mazes, Breakout variants
\end{itemize}


\subsection{Learning the environment latent space encoding}

\begin{itemize}
\item Need appropriate distance metric to determine when the
  controller has attained the goal, when the controller has strayed
  from the goal, or determining if the environment state has been
  encountered previously (backtracking detection for tree search)
\item Identify entities
\item Testing environments: Random mazes, Breakout variants, Other
  Atari games
\end{itemize}


\small
\bibliography{references}
\bibliographystyle{unsrt}

\end{document}
